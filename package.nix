{
  bash,
  fetchurl,
  lib,
  makeWrapper,
  rpm,
  stdenv,
}:
stdenv.mkDerivation rec {
  pname = "rpmrebuild";
  version = "2.18";

  src = fetchurl {
    url = "mirror://sourceforge/${pname}/${pname}-${version}.tar.gz";
    sha256 = "sha256-kZymJS6vv7WN1UsO+jyk5topM2STfc/jLDKjWyOKWDc=";
  };

  sourceRoot = ".";

  patches = [./absolute_paths.patch];
  postPatch = ''
    substituteAllInPlace rpmrebuild
  '';

  buildInputs = [makeWrapper];

  installFlags = "DESTDIR=$(out)";
  postInstall = ''
    wrapProgram "$out/bin/rpmrebuild" \
      --prefix PATH : "${lib.makeBinPath [bash rpm]}"
  '';

  meta = with lib; {
    description = "A tool to build an RPM file from a package that has already been installed";
    homepage = "https://sourceforge.net/projects/rpmrebuild";
    license = licenses.gpl2;
    maintainers = with maintainers; [b4dm4n];
    platforms = platforms.linux;
  };
}
